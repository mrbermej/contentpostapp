from webapp import webApp

FORM = """
    <hr>
    <form action="/" method="post">
      <div>
        <label>Resource: </label>
        <input type="text" name="resource" required>
      </div>
      <div>
        <label>Content: </label>
        <textarea name="content" rows="5" cols="33" required></textarea>
      </div>
      <div>
        <input type="submit" value="Submit">
      </div>
    </form>
"""

class contentapp(webApp):
    content = {'/': 'Pagina de inicio',
               '/page': 'Pagina sin contenido',
               '/adios': 'Adios Mundo Cruel'}

    def parse(self, request):
        method = request.split(' ', 2)[0]  # GET, POST, etc
        resource = request.split(' ', 2)[1]  # /index.html
        body_start = request.find('\r\n\r\n') + 4
        body = request[body_start:]
        return (method, resource, body)

    def process(self, parsed_request):
        (method, resource, body) = parsed_request
        if method == 'GET':
            if resource in self.content:
                code = '200 OK'
                html = "<html><body><h1>" + self.content[resource] + "</h1></body></html>"
            else:
                code = '404 Not Found'
                html = "<html><body> Not Found. Agrega tu mismo el recurso:"
                html += FORM
                html += "</body></html>"
                print(self.content)
            return (code, html)

        if method == 'POST':
            form_data = self.parse_form_data(body)
            resource = form_data['resource']
            content = form_data['content']
            self.content[resource] = content
            code = '200 OK'
            html = "<html><body><h1>Contenido actualizado:</h1>"
            html += "<p>Resource: {}</p>".format(resource)
            html += "<p>Content: {}</p>".format(content)
            html += "</body></html>"
            return (code, html)

    def parse_form_data(self, body):
        form_data = {}
        fields = body.split('&')
        for field in fields:
            key, value = field.split('=')
            form_data[key] = value.replace('+', ' ')
        return form_data

if __name__ == "__main__":
    contentapp("localhost", 1235)
